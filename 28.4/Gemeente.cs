﻿using System;

namespace _28._4
{
    class Gemeente
    {
        private string _postcode;
        private string _gemeenteNaam;

        public string Postcode { get; set; }
        public string GemeenteNaam { get; set; }

        public Gemeente(string postcode, string gemeente)
        {
            Postcode = postcode;
            GemeenteNaam = gemeente;
        }

        public override string ToString()
        {
            return Postcode + " - " + GemeenteNaam + Environment.NewLine;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
