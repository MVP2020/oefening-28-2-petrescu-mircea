﻿using System;

namespace _28._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Provincie provincie = new Provincie("Provincie Antwerpen");
            Console.WriteLine(provincie.ProvincieNaam);
            Console.ReadLine();


            Console.WriteLine("Please type your postcode:");
            string postcode = Console.ReadLine();

            Console.WriteLine("Please fill in the name of your commune");
            string naam = Console.ReadLine();

            Gemeente gemeente = new Gemeente(postcode, naam);
            provincie.AddGemeente(gemeente);

            Console.WriteLine("Please type your postcode:");
            string postcode2 = Console.ReadLine();

            Console.WriteLine("Please fill in the name of your commune");
            string naam2 = Console.ReadLine();

            Gemeente gemeente2 = new Gemeente(postcode2, naam2);
            provincie.AddGemeente(gemeente2);

            Console.WriteLine("Please type your postcode:");
            string postcode3 = Console.ReadLine();

            Console.WriteLine("Please fill in the name of your commune");
            string naam3 = Console.ReadLine();

            Gemeente gemeente3 = new Gemeente(postcode3, naam3);
            provincie.AddGemeente(gemeente3);

            //Console.WriteLine(provincie.ToString());

            Console.WriteLine(gemeente);

            Console.WriteLine(gemeente2);

            Console.WriteLine(gemeente3);

            provincie.RemoveGemeente(gemeente3);

            Console.WriteLine(Environment.NewLine + "Na het verwijderen van Brussel");

            Console.WriteLine(provincie.ProvincieNaam);

            Console.WriteLine(gemeente);

            Console.WriteLine(gemeente2);

            Console.ReadLine();

        }
    }
}
