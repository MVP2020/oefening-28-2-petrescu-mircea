﻿using System.Collections.Generic;

namespace _28._4
{
    class Provincie
    {
        private string _provincienaam;
        private List<Gemeente> gemeentes = new List<Gemeente>();
        private List<Gemeente> Gemeentes = new List<Gemeente>();

        public string ProvincieNaam { get; set; }
        public void AddGemeente(Gemeente gemeente)
        {
            Gemeentes.Add(gemeente);
        }

        public void RemoveGemeente(Gemeente gemeente)
        {
            Gemeentes.Remove(gemeente);
        }
        public Provincie(string provincie)
        {
            ProvincieNaam = provincie;
        }
        //public override string ToString()
        //{

        //    return Gemeentes.ToString();
        //}
    }
}
